"use strict";

document.addEventListener('DOMContentLoaded',setup);

function setup(){
    //Get all elements from the the Dom using a common class
    let delElm = document.getElementsByClassName('dlt');

    //Adding a eventListener to each of those elements
    for(let i of delElm){
        i.addEventListener('click',deletefnc);
    }

}

function deletefnc(e){
    e.target.parentElement.remove(); //You want to remove the element from the array instead of the DOM
}